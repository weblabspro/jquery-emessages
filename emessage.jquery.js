(function($) {

	$.emessage = Emessage = function(options) {
		var opts = $.extend({}, defaults, options);
		
		var template = opts.template
			.replace(/\{\{title\}\}/ig, opts.title)
			.replace(/\{\{body\}\}/ig, opts.body);

		var $modal = $(template),
			isOpened = true;

		if(!opts.buttons) {
			$modal.find('.modal-buttons').hide();
		}

		var closeModal = function() {
			if(isOpened) {
				isOpened = false;

				$modal.animate({
					'opacity': 0
				}, {
					duration: opts.duration,
					complete: function() {
						$modal.remove();
					}
				});

				if('function' === typeof opts.close) {
					opts.close();
				}
			}
		}, confirmModal = function() {
			if(isOpened && 'function' === typeof opts.confirm) {
				opts.confirm();
			}

			closeModal();
		}

		$('html').keydown(function(e) {
			if(e.which == 27){
			    closeModal();
			} else if(e.which == 13){
			    confirmModal();
			}
		});
		$modal.find('.close').click(function() {
			closeModal();
		});
		$modal.find('.cancel').click(function() {
			closeModal();
		});
		$modal.find('.confirm').click(function() {
			confirmModal();
		});

		$modal.click(function() {
			closeModal();
		});
		$modal.find('.modal').click(function(e) {
			e.stopPropagation();
		});

		$modal.css({
			'opacity': 0
		});

		$('body').find('.overlay').remove();
		$('body').append($modal);

		$modal.find('.modal-body').focus();

		$modal.animate({
			'opacity': 1
		}, opts.duration);
	};

	var defaults = {
		duration: 200,
		template: '<div class="overlay">'+
    '<div class="modal">'+
        '<div class="modal-head">'+
            '<div class="modal-title">{{title}}</div>'+
            '<div class="close"></div>'+
        '</div>'+
        '<div class="modal-body" tabindex="-1">'+
            '<div>{{body}}</div>'+
            '<div class="modal-buttons">'+
            	'<button class="modal-button cancel">Cancel</button>'+
            	'<button class="modal-button confirm">Confirm</button>'+
            '</div>'+
        '</div>'+
    '</div>'+
'</div>',
		title: 'Are you sure?',
		body: 'Confirm your action.',
		buttons: true
	};

})(jQuery);